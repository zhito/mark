from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
import csv

app = FastAPI()
templates = Jinja2Templates(directory="templates")
GOODS_FILE = "goods.csv"
ORDER_FILE = "orders.csv"
#чупапемюняню

def own_read_csv(file):
    data_array = []
    with open(file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count > 0:
                data_array.append([row[0], row[1]])
            line_count += 1
    return data_array


@app.get("/", response_class=HTMLResponse)
def index(request: Request):
    return templates.TemplateResponse("index.html", {"request": request, "goods": own_read_csv(GOODS_FILE)})


@app.get("/admin", response_class=HTMLResponse)
def admin(request: Request):
    return templates.TemplateResponse("admin.html", {"request": request, "orders": own_read_csv(ORDER_FILE)})


@app.get("/order", response_class=HTMLResponse)
def order(request: Request, name: str = None):
    return templates.TemplateResponse("order.html", {"request": request, "name": name})


@app.get("/buy", response_class=HTMLResponse)
def buy(order: str = None, name: str = None):
    if order:
        print(order)
        f = open(ORDER_FILE, "a")
        order += ',' + name + '\n'
        f.write(order)
        f.close()
        return HTMLResponse(content='<html><head><title>Good</title></head><body><h1>Good</h1></body></html>', status_code=200)
    else:
        return HTMLResponse(content='<html><head><title>Not Good</title></head><body><h1>Good</h1></body></html>', status_code=200)


