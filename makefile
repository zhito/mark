runserver:
	uvicorn main:app --reload
clone:
	git clone
add:
	git add
commit:
	git commit
push:
	git push
status:
	git status
venv:
	python3 -m venv venv
source:
	source venv/bin/activate
pull:
	git pull
